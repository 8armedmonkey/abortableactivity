package com.eightam.android.abortableactivity;

public interface Abortable {

    void abort();

    boolean isAborted();

}
