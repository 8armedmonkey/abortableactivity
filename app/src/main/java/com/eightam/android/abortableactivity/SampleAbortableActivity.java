package com.eightam.android.abortableactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.eightam.android.abortableactivity.DebugTags.TAG_ABORTABLE;
import static java.lang.String.format;

public class SampleAbortableActivity extends AbortableActivity {

    private static final String CLASS_NAME = SampleAbortableActivity.class.getSimpleName();

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SampleAbortableActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG_ABORTABLE, format("%s.onCreate enter", CLASS_NAME));
        super.onCreate(savedInstanceState);

        if (!isAborted()) {
            Log.d(TAG_ABORTABLE, format("%s.onCreate exit - not aborted", CLASS_NAME));
        } else {
            Log.d(TAG_ABORTABLE, format("%s.onCreate exit - aborted", CLASS_NAME));
        }
    }

    @Override
    protected void onCreateUnaborted(Bundle savedInstanceState) {
        Log.d(TAG_ABORTABLE, format("%s.onCreateUnaborted enter", CLASS_NAME));
        Log.d(TAG_ABORTABLE, format("%s.onCreateUnaborted exit", CLASS_NAME));
    }

    @Override
    protected void onStart() {
        Log.d(TAG_ABORTABLE, format("%s.onStart enter", CLASS_NAME));
        super.onStart();
        Log.d(TAG_ABORTABLE, format("%s.onStart exit", CLASS_NAME));
    }

    @Override
    protected void onResume() {
        Log.d(TAG_ABORTABLE, format("%s.onResume enter", CLASS_NAME));
        super.onResume();
        Log.d(TAG_ABORTABLE, format("%s.onResume exit", CLASS_NAME));
    }

    @Override
    protected void onPause() {
        Log.d(TAG_ABORTABLE, format("%s.onPause enter", CLASS_NAME));
        super.onPause();
        Log.d(TAG_ABORTABLE, format("%s.onPause exit", CLASS_NAME));
    }

    @Override
    protected void onStop() {
        Log.d(TAG_ABORTABLE, format("%s.onStop enter", CLASS_NAME));
        super.onStop();
        Log.d(TAG_ABORTABLE, format("%s.onStop exit", CLASS_NAME));
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG_ABORTABLE, format("%s.onDestroy enter", CLASS_NAME));
        super.onDestroy();
        Log.d(TAG_ABORTABLE, format("%s.onDestroy exit", CLASS_NAME));
    }

}
