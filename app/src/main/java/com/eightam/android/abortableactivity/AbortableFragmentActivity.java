package com.eightam.android.abortableactivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

public abstract class AbortableFragmentActivity extends FragmentActivity implements Abortable {

    protected boolean activityAborted;

    @Override
    public void abort() {
        activityAborted = true;
    }

    @Override
    public boolean isAborted() {
        return activityAborted;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (activityAborted) {
            finish();
        } else {
            onCreateUnaborted(savedInstanceState);
        }
    }

    protected abstract void onCreateUnaborted(Bundle savedInstanceState);

}
