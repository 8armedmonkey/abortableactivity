package com.eightam.android.abortableactivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;

import butterknife.OnClick;

import static butterknife.ButterKnife.bind;
import static butterknife.ButterKnife.findById;

public class MainActivity extends AppCompatActivity {

    protected ActivityAbortManager activityAbortManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        bind(this);
    }

    @OnClick({R.id.radioAbortYes, R.id.radioAbortNo})
    protected void onAbortPreferenceChange() {
        boolean shouldAbort = ((RadioButton) findById(this, R.id.radioAbortYes)).isChecked();
        activityAbortManager.setShouldAbortNonAllowedActivities(shouldAbort);
    }

    @OnClick(R.id.buttonOpenActivity)
    protected void onOpenActivity() {
        startActivity(SampleAbortableActivity.getStartIntent(this));
    }

    @OnClick(R.id.buttonOpenFragmentActivity)
    protected void onOpenFragmentActivity() {
        startActivity(SampleAbortableFragmentActivity.getStartIntent(this));
    }

    @OnClick(R.id.buttonOpenAppCompatActivity)
    protected void onOpenAppCompatActivity() {
        startActivity(SampleAbortableAppCompatActivity.getStartIntent(this));
    }

    @OnClick(R.id.buttonOpenAllowedActivity)
    protected void onOpenAllowedActivity() {
        startActivity(SampleAllowedActivity.getStartIntent(this));
    }

    private void initialize() {
        activityAbortManager = ((Application) getApplication()).getActivityAbortManager();
    }

}
