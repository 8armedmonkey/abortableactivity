package com.eightam.android.abortableactivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

public abstract class AbortableActivity extends Activity implements Abortable {

    protected boolean activityAborted = false;

    @Override
    public void abort() {
        activityAborted = true;
    }

    @Override
    public boolean isAborted() {
        return activityAborted;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (activityAborted) {
            finish();
        } else {
            onCreateUnaborted(savedInstanceState);
        }
    }

    protected abstract void onCreateUnaborted(Bundle savedInstanceState);

}
