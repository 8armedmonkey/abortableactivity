package com.eightam.android.abortableactivity;

import android.app.Activity;
import android.os.Bundle;

import java.util.HashSet;
import java.util.Set;

public class ActivityAbortManager extends SimpleActivityLifecycleCallbacks {

    private Set<Class<? extends Activity>> allowedActivities;
    private boolean shouldAbortNonAllowedActivities;

    public ActivityAbortManager() {
        this(false);
    }

    public ActivityAbortManager(boolean shouldAbortNonAllowedActivities) {
        this.allowedActivities = new HashSet<>();
        this.shouldAbortNonAllowedActivities = shouldAbortNonAllowedActivities;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Class<? extends Activity> activityClass = activity.getClass();

        if (shouldAbortNonAllowedActivities && !isActivityAllowed(activityClass)) {
            if (activity instanceof Abortable) {
                ((Abortable) activity).abort();
            } else {
                activity.finish();
            }
        }
    }

    public void registerAllowedActivities(Class<? extends Activity> activityClass) {
        allowedActivities.add(activityClass);
    }

    public void unregisterAllowedActivities(Class<? extends Activity> activityClass) {
        allowedActivities.remove(activityClass);
    }

    public void clearAllowedActivities() {
        allowedActivities.clear();
    }

    public boolean isActivityAllowed(Class<? extends Activity> activityClass) {
        if (allowedActivities.contains(activityClass)) {
            return true;
        } else {
            for (Class<? extends Activity> c : allowedActivities) {
                if (c.isAssignableFrom(activityClass)) {
                    return true;
                }
            }
            return false;
        }
    }

    public boolean shouldAbortNonAllowedActivities() {
        return shouldAbortNonAllowedActivities;
    }

    public void setShouldAbortNonAllowedActivities(boolean shouldAbort) {
        shouldAbortNonAllowedActivities = shouldAbort;
    }

}
