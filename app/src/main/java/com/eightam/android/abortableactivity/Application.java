package com.eightam.android.abortableactivity;

public class Application extends android.app.Application {

    private ActivityAbortManager activityAbortManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
        registerAllowedActivities();
        registerActivityLifecycleCallbacks(activityAbortManager);
    }

    public ActivityAbortManager getActivityAbortManager() {
        return activityAbortManager;
    }

    private void initialize() {
        activityAbortManager = new ActivityAbortManager();
    }

    private void registerAllowedActivities() {
        activityAbortManager.registerAllowedActivities(SampleAllowedActivity.class);
    }

}
