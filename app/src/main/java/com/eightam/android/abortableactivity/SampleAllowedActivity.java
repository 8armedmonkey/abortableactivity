package com.eightam.android.abortableactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class SampleAllowedActivity extends AppCompatActivity {

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SampleAllowedActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
